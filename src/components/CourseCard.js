import {useState} from 'react';
import {Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({course}) {

	const {name, description, price, _id} = course;

	// Use the state hook for this component to be able to store its state
	// States are used to keep track of information realted to individual components
	/*
		SYNTAX
		const [getter, setter] = useState(initalGetterValue)
	*/
	// const [count, setCount] = useState(0);
	// const [seats, setSeats] = useState(30);
	// const [isOpen, setIsOpen] = useState(true);
	

	// function enroll() {
    // 	if(seats > 0){
    //   		setCount(count + 1)
    //   		setSeats(seats - 1)
    // 	} else {
    //   		alert("No more seats.")
    // 	}
    
	// function enroll() {
	// 	setCount(count + 1);
    //    	setSeats(seats - 1);
	// }
	// useEffect() - will allow us to execute a function if the value of seats state changes.
	// useEffect(() => {
	// 	if(seats === 0) {
	// 		setIsOpen(false);
	// 		document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
	// 	}
	// 	// Will run anytime one of the values in the array of the dependencies changes.
	// }, [seats]);
	



	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>PhP {price}</Card.Text>
	        {/*<Card.Text>Enrollees: {count}</Card.Text>*/}
	        {/*<Button variant="primary">Enroll</Button>*/}
	        {/*<Card.Text>Seats: {seats}</Card.Text>
	        <Button id={`btn-enroll-${_id}`} className="bg-primary" onClick={enroll}>Enroll</Button>*/}
	        <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
	    </Card.Body>
	</Card>
	)
}