import {Link} from 'react-router-dom';
import Banner from '../components/Banner';

export default function Error() {

	const data = {
		title : "Error 404 - Not Found",
		content : "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back to home"
	}

	return (
		<Banner data={data}/>

		/*<>
			<h1>Zuitt Booking</h1>
			<p>That page was not found :(</p>
			<Link to="/">Go back to homepage</Link>
		</>*/
		
	)
}
